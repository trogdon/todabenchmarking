# Data to be used for benchmarking Toda lattices.

# By Deniz Bilman and Thomas Trogdon

See the Mathematica notebook (AnalyzeData.nb) for directions on the use of the data.  Use your favorite language and time-stepping routine to create the data and then analyze it.
